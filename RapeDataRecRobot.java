
package rapedatarecrobot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

/**
 *
 * @author wunmi
 */
public class RapeDataRecRobot {
    
    Random rand2 = new Random();
    Random rand = new Random();
    final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    //final String DRIVER = "org.apache.derby.jdbc.clientDriver";
    final String DATABASE_URL = "jdbc:derby://localhost:1527/RapeDataRecDB;create=true;";
    Connection connection = null;
    Statement statement = null;
    ResultSet rset = null;
    int victimLow = 7;
    int victimHigh = 85;
    int suspectLow = 17;
    int suspectHigh = 65;
   // int age;

    public void loadDB() throws SQLException {
        try {
            Class.forName(DRIVER); //load the driver

            System.out.println("driver loaded   ... ok");
            connection = DriverManager.getConnection(DATABASE_URL, "wunmi", "wunmi");
            //connection = DriverManager.getConnection("jdbc:derby:myDB");
            System.out.println("connection successful!!!");
            statement = connection.createStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
          }
        
                     
        String victim[]={"I_Male", "I_Female","W_Male",
            "W-Female","B_Male","B_Female","C_Male","C_Female"};
        
        String suspect[]={"I_Male", "I_Female","W_Male",
            "W-Female","B_Male","B_Female","C_Male","C_Female"};
        //Victim Age, Suspect Age
        
        String incidentTime[] = {"Morning","Noon","Evening","Night"};
        
        String incidentDay[] ={"Monday", "Tuesday","Wednesday",
                        "Thursday","Friday","Saturday","Sunday"}; 
        
        String incidentMonth[]={"January","February","March","April","May","June",
                        "July","August","September","October","November","December"};
        
        String incidentYear[]={"2009","2010","2011","2012","2013","2014"};
                
        String incident_loc[] = {"Mitchells_Plain", "CapeTown_Central", "Nyanga","Bishop_Lavis",
            "Milnerton", "Manenberg", "Delft","Gugulethu","Athlone","Grassy_Park","Elsies_River",
            "Mfuleni","Harare","Goodwood","Woodstock","Philippi_Central","Phillippi_East","Claremont",
            "Sea Point","Lansdowne","Steenberg","Wynberg","Dieprivier","Lingelethu_West","Kirstenhof",
            "Langa","Maitland","Pinelands","Rondebosch","Mowbray","Khayelitsha", "Muizenberg",
            "Hout_Bay","Fish_Hoek", "NoordHoek","Parow","Mfuleni","Kraaifontein","Strand", "Atlantis"};

        String motivation[] ={"Revenge","Jealousy","Self-Satisfaction","Opportunist"};
        
        String suspectFrame[] ={"Slender","Moderate","Fat"};
        String suspectHeight[]={"Tall", "Medium", "Short"};
        String evidence[] ={"Footprint","Fingerprint","Siemen","Hair","CigarretteType"};
        String modus_operandi[]={"Lure","Kidnap","Deceit","Substance_Influence",
                                "knife","Gun","Vehicle","PepperSpray"};
        String idKit[]={"Masked","Unmasked","Tattoo_on_Face","Tattoo_on_Leg",
                "Tattoo_on_neck","Tattoo_on_arm"};        
        
        
        for (int i = 0; i < 8000; i++) {
            int constRand = rand.nextInt(3);
            //int constRand2 = rand.nextInt(93);
            String sql = "insert into WUNMI.RAPEDATARECDB values('" + incidentDay[rand.nextInt(7)] + "', '"+ 
                    incidentTime[rand.nextInt(4)] +"', '" + incidentMonth[rand.nextInt(12)] + "','" + 
                     incidentYear[rand2.nextInt(6)] +"', '" + incident_loc[rand.nextInt(40)] +
                    "','" +victim[rand.nextInt(8)] + "','" +suspect[rand.nextInt(8)] +  "','" + 
                    rand2.nextInt((victimHigh-victimLow) + victimLow + 1)+  "','" + 
                    rand2.nextInt((suspectHigh-suspectLow) + suspectLow + 1)+
                    "','" + suspectFrame[rand.nextInt(3)] + "','" + suspectHeight[rand.nextInt(3)] + 
                    "','"+ motivation[rand.nextInt(4)] + "','" + modus_operandi[rand.nextInt(8)] + "','" + 
                    idKit[rand.nextInt(6)] + "','" + evidence[rand.nextInt(5)] + "')";
            
            System.out.println(sql + "\n");
            statement.executeUpdate(sql);
        }  
          
    }
    
    
    public static void main(String[] args) throws ClassNotFoundException, SQLException{
        
        RapeDataRecRobot app = new RapeDataRecRobot();
        app.loadDB();

    
        // TODO code application logic here
    }
    
}
