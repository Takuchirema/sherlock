//Initial servlet 
import java.io.IOException;
import java.io.PrintWriter;

//javac -cp .;C:\Tomcat7\lib\servlet-api.jar
//javac -cp .;C:\xampp\tomcat\lib\servlet-api.jar C:\Users\Tebogo\Desktop\Capstone\rd\src\LoginDB.java

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
		description = "Login Servlet", 
		urlPatterns = { "/FirstServlet" }, 
		initParams = { 
				@WebInitParam(name = "name", value = "admin15"), 
				@WebInitParam(name = "password", value = "Sherlock")
		})
public class FirstServlet extends HttpServlet {

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
            try (PrintWriter out = response.getWriter()) {
                String n=request.getParameter("name");
                String p=request.getParameter("password");
                
                String userID = getServletConfig().getInitParameter("name");
		String password = getServletConfig().getInitParameter("password");

                
                log("name="+n+"::password="+p);
                
                if(userID.equals(n) && password.equals(p)){
			response.sendRedirect("index.html");
                
                if(LoginDB.validate(n, p)){
                    RequestDispatcher rd=request.getRequestDispatcher("login.html");
                    //rd.forward(request,response);
                }
                else{
                    out.print("Sorry username or password error");
                    RequestDispatcher rd=request.getRequestDispatcher("login.html");
                    rd.include(request,response);
                }
            }
	}

    }
}
