//Updates the database 
import java.sql.*;

public class LoginDB {
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

	static final String DB_URL ="jdbc:mysql://127.0.0.1:3306/crimedata";
	static final String username="tebogo";
	static final String paswd="password";
	static final String table = "usertable";

   public static boolean validate(String name,String password)
   {
      boolean status=false;
      try
         {
         	Class.forName("com.mysql.jdbc.Driver");
         	Connection con=DriverManager.getConnection(DB_URL, username, paswd );
         	
         	PreparedStatement ps=con.prepareStatement("select * from usertable where name=? and password=?");
         	ps.setString(1,name);
         	ps.setString(2,password);
         	
         	ResultSet rs=ps.executeQuery();
         	status=rs.next();
         }
      catch(ClassNotFoundException | SQLException e)
         {
            System.out.println(e);
         }
         
      return status;
   }
}
