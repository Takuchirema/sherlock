import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;


public class Statistics {
	
	private  ArrayList<Graph> clusters;
	private  ArrayList<CrimeNode> graph;
	
	//the key in outer map is the type of statistic
	//the key in inner map is the suburb with the crimes and int is number of crimes
	private  HashMap<String,HashMap<String,ArrayList<CrimeNode> > > graphStatistics = 
			new HashMap<String,HashMap<String,ArrayList<CrimeNode> > >() ;
	
	//the key is the location and the list contains the clusters in that location
	private  HashMap<String,ArrayList<Cluster>> clusterStatistics = new HashMap<String,ArrayList<Cluster>>() ;
	
	public Statistics(ArrayList<Graph> clusters,ArrayList<CrimeNode> graph){
		this.clusters = clusters;
		this.graph = graph;
	}
	
	public  void createStatistics() throws FileNotFoundException{
		//create a HashMap with the type of statistic in the map
		//graphStatistics is hashmap with key of a location which had crimes in them
		
		//can have crimes which has all the crimes in a location (graph iteration)
		//can have Night,Noon,Evening,Morning for the times (graph iteration)
		//can have Monday - Sunday for the days             (graph iteration)
		
		//can create cluster statistics which shows each location with clusters present in it
		
		createClusterStatistics();
		
		createGraphStatistics();
		
		System.out.println("GRAPH STATS\n");
		
		printGraphStatistics(graphStatistics);
		
		System.out.println("CLUSTER STATS\n");
		
		printClusterStatistics(clusterStatistics);
	}
	
	public void createClusterStatistics(){
		
		for (int i=0;i<clusters.size();i++){
			
			Cluster cluster = new Cluster(clusters.get(i).getCrimeNodes(),"gang"+(i+1));
			
			//all the locations the cluster is working on
			HashMap<String,ArrayList<CrimeNode>> crimesCommitted = cluster.createStatistics();
			
			//iterate the locations putting the cluster in those
			for ( String key : crimesCommitted.keySet() ) {
			    //System.out.println( key );
			    if (clusterStatistics.containsKey(key)){
			    	clusterStatistics.get(key).add(cluster);
			    }else{
			    	ArrayList<Cluster> loc = new ArrayList<Cluster>();
			    	loc.add(cluster);
			    	clusterStatistics.put(key, loc);
			    }
			}
			
		}
		
	}
	
	public  void createGraphStatistics(){
		//graphStatistics.put("location", new HashMap<String,ArrayList<CrimeNode> >());
		
		for (int i=0;i<graph.size();i++){
			
			CrimeNode crime = graph.get(i);
			
			//put in the locations
			if (graphStatistics.containsKey(crime.incident_loc)){
				
				graphStatistics.get(crime.incident_loc).get("crimes").add(crime);
				
				//add the crime to graphStatistics
				putCrimeNode(crime);
				
			}else{
				HashMap<String,ArrayList<CrimeNode>> stats = new HashMap<String,ArrayList<CrimeNode>>();
				
				stats.put("crimes", new ArrayList<CrimeNode>() );
				
				stats.put("morning", new ArrayList<CrimeNode>() );
				stats.put("noon", new ArrayList<CrimeNode>() );
				stats.put("evening", new ArrayList<CrimeNode>() );
				stats.put("night", new ArrayList<CrimeNode>() );
				
				stats.put("Sunday", new ArrayList<CrimeNode>() );
				stats.put("Monday", new ArrayList<CrimeNode>() );
				stats.put("Tuesday", new ArrayList<CrimeNode>() );
				stats.put("Wednesday", new ArrayList<CrimeNode>() );
				stats.put("Thursday", new ArrayList<CrimeNode>() );
				stats.put("Friday", new ArrayList<CrimeNode>() );
				stats.put("Saturday", new ArrayList<CrimeNode>() );
				
				graphStatistics.put(crime.incident_loc, stats);
				
				//add to the crimes type
				graphStatistics.get(crime.incident_loc).get("crimes").add(crime);
				
				//add the crime to graphStatistics
				putCrimeNode(crime);
			 }
			
		}
		
	}
	
	public  void putCrimeNode(CrimeNode crime){
		
		//add the times array
		if (crime.incident_time.equals("Morning"))
			graphStatistics.get(crime.incident_loc).get("morning").add(crime);
		else if (crime.incident_time.equals("Noon"))
			graphStatistics.get(crime.incident_loc).get("noon").add(crime);
		else if (crime.incident_time.equals("Evening"))
			graphStatistics.get(crime.incident_loc).get("evening").add(crime);
		else if (crime.incident_time.equals("Night"))
			graphStatistics.get(crime.incident_loc).get("night").add(crime);
		
		//add the days array
		if (crime.incident_day.equals("Sunday"))
			graphStatistics.get(crime.incident_loc).get("Sunday").add(crime);
		else if (crime.incident_day.equals("Monday"))
			graphStatistics.get(crime.incident_loc).get("Monday").add(crime);
		else if (crime.incident_day.equals("Tuesday"))
			graphStatistics.get(crime.incident_loc).get("Tuesday").add(crime);
		else if (crime.incident_day.equals("Wednesday"))
			graphStatistics.get(crime.incident_loc).get("Wednesday").add(crime);
		else if (crime.incident_day.equals("Thursday"))
			graphStatistics.get(crime.incident_loc).get("Thursday").add(crime);
		else if (crime.incident_day.equals("Friday"))
			graphStatistics.get(crime.incident_loc).get("Friday").add(crime);
		else if (crime.incident_day.equals("Saturday"))
			graphStatistics.get(crime.incident_loc).get("Saturday").add(crime);
		
	}
	
	public void printClusterStatistics(HashMap<String,ArrayList<Cluster>> stats) throws FileNotFoundException{
		
		//make a string with variables e.g. var locs = {Rosebank : {gang1 :'100%'} };
		//for the javascript
		String script = "{";
		
		//iterate the locations putting the cluster in those
		for ( String location : clusterStatistics.keySet() ) {
		    //System.out.println( "\n******** "+location+" ********\n" );
		    
		    //get total crimes committed in a location
		    int crimeNo = getTotalCrimes(stats,location);
		    
		    ArrayList<Cluster> type = clusterStatistics.get(location);
		    
		    script = script +""+location.replaceAll("[^a-zA-Z_]", "")+" : { ";
		    
		    for (int i=0;i<type.size();i++) {
		    	
		    	String html = type.get(i).getHTML();
		    	
                        String coordinates = type.get(i).getCoordinates();
                        
		    	int locNo = type.get(i).getLocCrimes(location).size();
		    	
		    	//get percentage of crimes done by gang in this location
		    	double perc = ( (double)locNo/crimeNo )*100;
		    	
		    	//System.out.println( "-- "+type.get(i).clusterName+"\n");
		    	
		    	script = script+""+type.get(i).clusterName+" : {perc: '"+(int)perc+"',"
                                + "coordinates:'"+coordinates+"',html:'"+html+"'},";
		    	//printCrimes(type.get(i).getCrimes());
		    }
		    
		    script = script.substring(0, script.length() -1);
		    script = script + "},";
		    
		    //System.out.println(script);
		}
		
		script = script.substring(0, script.length() -1);
	    script = script + "}";
	    
	    System.out.println(script);
	    
	    PrintWriter out = new PrintWriter("C:/Users/User/Desktop/Notes/Honours/Visual_Thinking_and_Visualisation/project/visual/Capstone-final/build/web/js/ClusterStatistics.txt");
	    out.println(script);
	    out.close();
		
	}
	
	public int getTotalCrimes(HashMap<String,ArrayList<Cluster>> stats,String loc){
		int total = 0;
		
		ArrayList<Cluster> clusters = stats.get(loc);
		
		for (int i=0;i<clusters.size();i++){
			total = total + clusters.get(i).getLocCrimes(loc).size();
		}
		
		return total;
	}
	
	public  void printGraphStatistics(HashMap<String,HashMap<String,ArrayList<CrimeNode> > > stats) throws FileNotFoundException{
		//make a string with variables e.g. 
		//var locationStatistics = {Rosebank : {morning : 5,..,night:5,sunday:5,..,saturday:5} };
		//var crimes=300; var morning=10;..; var night=30; var sunday=30;..; saturday=20;
		
		int crimes=0;
		
		int morning=0,noon = 0,evening=0,night=0;
		
		int sunday=0,monday=0,tuesday=0,wednesday=0,thursday=0,friday=0,saturday=0;
		
		String script = "{";
		//iterate the locations putting the cluster in those
		for ( String location : graphStatistics.keySet() ) {
		    //System.out.println( "\n******** "+location+" ********\n" );
		    HashMap<String,ArrayList<CrimeNode>> type = graphStatistics.get(location);
		    
		    script = script +""+location.replaceAll("[^a-zA-Z_]", "")+" : { ";
		    
		    for ( String stat : type.keySet() ) {
		    	//System.out.println( "-- "+stat+": "+type.get(stat).size() );
		    	script = script+""+stat+" : "+type.get(stat).size()+",";
		    	
		    	//add the statistics to the running total
		    	switch (stat){
		    		case "Morning":
		    			morning += type.get(stat).size();
		    			break;
		    		case "Noon":
		    			noon += type.get(stat).size();
		    			break;
		    		case "Evening":
		    			evening += type.get(stat).size();
		    			break;
		    		case "Night":
		    			night += type.get(stat).size();
		    			break;
		    		case "Sunday":
		    			sunday += type.get(stat).size();
		    			break;
		    		case "Monday":
		    			monday += type.get(stat).size();
		    			break;
		    		case "Tuesday":
		    			tuesday += type.get(stat).size();
		    			break;
		    		case "Wednesday":
		    			wednesday += type.get(stat).size();
		    			break;
		    		case "Thursday":
		    			thursday += type.get(stat).size();
		    			break;
		    		case "Friday":
		    			friday += type.get(stat).size();
		    			break;
		    		case "Saturday":
		    			saturday += type.get(stat).size();
		    			break;
		    		case "crimes":
		    			crimes += type.get(stat).size();
		    			break;
		    		default:
		    			break;
		    		
		    	}
		    			
		    }
		    
		    script = script.substring(0, script.length() -1);
		    script = script + "},";
		    
		    //System.out.println(script);
		}
		
		script = script.substring(0, script.length() -1);
	    script = script + "}";
	    
	    String totals = "crimes="+crimes+"\n"+"morning="+morning+"\n"+"noon="+noon+"\n"+
				"evening="+evening+"\n"+"night="+night+"\n"+"sunday="+sunday+"\n"+
				"monday="+monday+"\n"+"tuesday="+tuesday+"\n"+"wednesday="+wednesday+"\n"+
				"thursday="+thursday+"\n"+"friday="+friday+"\n"+"saturday="+saturday+"\n";

	    System.out.println(script);
	    System.out.println(totals);
	    
	    PrintWriter out = new PrintWriter("C:/Users/User/Desktop/Notes/Honours/Visual_Thinking_and_Visualisation/project/visual/Capstone-final/build/web/js/GraphStatistics.txt");
	    out.println(script);
	    out.close();
	    
	    out = new PrintWriter("C:/Users/User/Desktop/Notes/Honours/Visual_Thinking_and_Visualisation/project/visual/Capstone-final/build/web/js/GraphTotals.txt");
	    out.println(totals);
	    out.close();
	    
	}
	
	public static void printCrimes(ArrayList<CrimeNode> crimes){
		
		
		for (int i=0;i<crimes.size();i++){
			System.out.println((i+1)+
					String.format("%0$5s", ". ID: "+crimes.get(i).id)+
					String.format("%0$25s", ", Location: "+crimes.get(i).incident_loc)+					
					String.format("%0$17s",", Day :"+crimes.get(i).incident_day)+
					String.format("%0$14s",", Time :"+crimes.get(i).incident_time) );
		}
	}

}
