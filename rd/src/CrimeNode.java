import java.util.ArrayList;
import java.util.Arrays;


public class CrimeNode implements Comparable<CrimeNode>{
	
	public int id;
	public String incident_day;
	public String incident_time;
	public String victim;
	public String suspect;
	public String victim_age;
	public String suspect_age;
	public String method_capture;
	public String substance_abuse;
	public String suspect_disguised;
	public String incident_loc;
        public String motivation;
        public String suspect_frame;
        
        public String lat;
        public String lng;
        public String type;
	
	public int connected =0;
	
	public ArrayList<CrimeNode> MasterNode = new ArrayList<CrimeNode>(Arrays.asList(this));
	
	public CrimeNode(int id){
		this.id = id;
	}
	
	public int getId()
	{
		return id;
	}

        @Override
        public int compareTo(CrimeNode crime) {
            return crime.incident_loc.compareTo(incident_loc);
        }

}
