import java.sql.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
 
public class CrimeCollection
{
	 
    /* This is for Visualisation. Putting the data in order */
    public static ArrayList<CrimeNode> CrimeList;
     
    private HashMap<String,ArrayList<Integer>> crimes_location = new HashMap<String,ArrayList<Integer>>();
    
    private HashMap<String,ArrayList<Integer> > crimes_day = new HashMap<String,ArrayList<Integer> >();
    
    private HashMap<String,ArrayList<Integer>> crimes_time = new HashMap<String,ArrayList<Integer>>();
     
    private HashMap<String,ArrayList<Integer>> crimes_type = new HashMap<String,ArrayList<Integer>>();
    
    /* ******************************************************/
    
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL ="jdbc:mysql://127.0.0.1:3306/crimedata";
    static final String DB_URL2 ="jdbc:mysql://127.0.0.1:3306/cluster";
    static final String username="tebogo";
    static final String paswd="password";
    static final String table = "table1";
    static final int columNo = 12;
    static Statement stat = null;
    static Connection con =null;
    String sql = "";
    static ResultSet crimes =null;
    public static Graph graph;
   
    public static ArrayList<CrimeNode> GraphCrimes =null;
    static Statement statement = null;
    static Connection connect =null;
    static String sQl ;
    static ResultSet graphTable;
    static HashMap<String,String> locations = new HashMap<String,String>();

    public void createGraph()
    {
        locations.put("1","ROGERS_PARK");
        locations.put("2","WEST_RIDGE");
        locations.put("3","UPTOWN");
        locations.put("4","LINCOLN_SQUARE");
        locations.put("5","NORTH_CENTER");
        locations.put("6","LAKE_VIEW");
        locations.put("7","LINCOLN_PARK");
        locations.put("8","NEAR_NORTH_SIDE");
        locations.put("9","EDISON_PARK");
        locations.put("10","NORWOOD_PARK");
        locations.put("11","JEFFERSON_PARK");
        locations.put("12","FOREST_GLEN");
        locations.put("13","NORTH_PARK");
        locations.put("14","ALBANY_PARK");
        locations.put("15","PORTAGE_PARK");
        locations.put("16","IRVING_PARK");
        locations.put("17","DUNNING");
        locations.put("18","MONTCLARE");
        locations.put("19","BELMONT_CRAGIN");
        locations.put("20","HERMOSA");
        locations.put("21","AVONDALE");
        locations.put("22","LOGAN_SQUARE");
        locations.put("23","HUMBOLDT_PARK");
        locations.put("24","WEST_TOWN");
        locations.put("25","AUSTIN");
        locations.put("26","WEST_GARFIELD_PARK");
        locations.put("27","EAST_GARFIELD_PARK");
        locations.put("28","NEAR_WEST_SIDE");
        locations.put("29","NORTH_LAWNDALE");
        locations.put("30","SOUTH_LAWNDALE");
        locations.put("31","LOWER_WEST_SIDE");
        locations.put("32","LOOP");
        locations.put("33","NEAR_SOUTH_SIDE");
        locations.put("34","ARMOUR_SQUARE");
        locations.put("35","DOUGLAS");
        locations.put("36","OAKLAND");
        locations.put("37","FULLER_PARK");
        locations.put("38","GRAND_BOULEVARD");
        locations.put("39","KENWOOD");
        locations.put("40","WASHINGTON_PARK");
        locations.put("41","HYDE_PARK");
        locations.put("42","WOODLAWN");
        locations.put("43","SOUTH_SHORE");
        locations.put("44","CHATHAM");
        locations.put("45","AVALON_PARK");
        locations.put("46","SOUTH_CHICAGO");
        locations.put("47","BURNSIDE");
        locations.put("48","CALUMET_HEIGHTS");
        locations.put("49","ROSELAND");
        locations.put("50","PULLMAN");
        locations.put("51","SOUTH_DEERING");
        locations.put("52","EAST_SIDE");
        locations.put("53","WEST_PULLMAN");
        locations.put("54","RIVERDALE");
        locations.put("55","HEGEWISCH");
        locations.put("56","GARFIELD_RIDGE");
        locations.put("57","ARCHER_HEIGHTS");
        locations.put("58","BRIGHTON_PARK");
        locations.put("59","MCKINLEY_PARK");
        locations.put("60","BRIDGEPORT");
        locations.put("61","NEW_CITY");
        locations.put("62","WEST_ELSDON");
        locations.put("63","GAGE_PARK");
        locations.put("64","CLEARING");
        locations.put("65","WEST_LAWN");
        locations.put("66","CHICAGO_LAWN");
        locations.put("67","WEST_ENGLEWOOD");
        locations.put("68","ENGLEWOOD");
        locations.put("69","GREATER_GRAND_CROSSING");
        locations.put("70","ASHBURN");
        locations.put("71","AUBURN_GRESHAM");
        locations.put("72","BEVERLY");
        locations.put("73","WASHINGTON_HEIGHTS");
        locations.put("74","MOUNT_GREENWOOD");
        locations.put("75","MORGAN_PARK");
        locations.put("76","OHARE");
        locations.put("77","EDGEWATER");

    try{

            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to the Database");
            con = DriverManager.getConnection(DB_URL,username,paswd);

            System.out.println("Successfully Connected");
            stat = con.createStatement();
            String sqlQuery = "SELECT * FROM crime_table_CH1000";
            System.out.println(stat.executeQuery(sqlQuery));

            crimes =stat.executeQuery(sqlQuery);
            int CrimeNodeID = 0;

            CrimeList = new ArrayList<>();

            String dayNames[] = new DateFormatSymbols().getWeekdays();
            while (crimes.next())
            {


                if (crimes.getString ("Latitude") != null && crimes.getString ("Date") != null
                        && crimes.getString ("Primary Type") != null && crimes.getString ("Community Area") != null
                        && crimes.getString ("Longitude") != null && !crimes.getString ("Latitude").equals("") 
                        && !crimes.getString ("Date").equals("")&& !crimes.getString ("Primary Type").equals("") 
                        && !crimes.getString ("Community Area").equals("") && !crimes.getString ("Longitude").equals("") ){

                    CrimeNodeID ++;
                    CrimeNode crime = new CrimeNode(CrimeNodeID);

                    String[] dates = crimes.getString("Date").split(" ");
                    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                    dateFormat.setLenient(false);
                    //java.sql.Date sqlDate = new java.sql.Date(dates[0]+" "+dates[1]);
                    Date date = dateFormat.parse(dates[0]+" "+dates[1]);

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    String day = dayNames[cal.get(Calendar.DAY_OF_WEEK)];

                    int hour = cal.get(Calendar.HOUR_OF_DAY);
                    String time = timeOfDay(hour);

                     //System.out.println("Day "+day+
                            //"Time "+time);

                    crime.incident_day = day;
                    crime.incident_time = time;
                    crime.type = crimes.getString ("Primary Type").replaceAll(" ", "_");;
                    crime.incident_loc=locations.get(crimes.getString("Community Area"));

                    crime.lat = crimes.getString ("Latitude");
                    crime.lng = crimes.getString ("Longitude");
                    
                    CrimeList.add(crime);
                    
                    if (crimes_location.containsKey(crime.incident_loc))
                        crimes_location.get(crime.incident_loc).add(crime.id-1);
                    else
                        crimes_location.put(crime.incident_loc,new ArrayList<>());
                    
                    if (crimes_day.containsKey(crime.incident_day))
                        crimes_day.get(crime.incident_day).add(crime.id-1);
                    else
                        crimes_day.put(crime.incident_day,new ArrayList<>());
                   
                    if (crimes_time.containsKey(crime.incident_time))
                        crimes_time.get(crime.incident_time).add(crime.id-1);
                    else
                        crimes_time.put(crime.incident_time,new ArrayList<>());

                    if (crimes_type.containsKey(crime.type))
                        crimes_type.get(crime.type).add(crime.id-1);
                    else
                        crimes_type.put(crime.type,new ArrayList<>());
                    
    
                }


                    //System.out.println(incident_day+ " " + incident_time + " " + victim +" "+ suspect+ " " + victim_age + " " + suspect_age + " "+ method_capture + " " + substance_abuse + " " + suspect_disguised + " "+ incident_loc);
             }
            
            VisStatistics statistics = new VisStatistics (CrimeList,crimes_location,crimes_day,crimes_time,crimes_type);
            statistics.createStatistics();
            
            graph = new Graph(CrimeList, null);

       }
       catch(Exception e){System.out.println(e);}

    }

    public String timeOfDay(int timeOfDay){
        String time = "";
        if(timeOfDay >= 0 && timeOfDay < 12){
            time = "Morning";        
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            time = "Noon";
        }else if(timeOfDay >= 16 && timeOfDay < 21){
            time = "Evening";
        }else if(timeOfDay >= 21 && timeOfDay < 24){
            time = "Night";
        }
        return time;
    }

    public ArrayList<CrimeNode> getCrimeNodes()
    {
            return CrimeList;
    }

    public Graph getGraph()
    {
            return graph;
    }

    public static void saveGraph(ArrayList<Graph> graphs) throws SQLException, ClassNotFoundException
    { 
        Class.forName(JDBC_DRIVER);
        ArrayList<Graph> gr = graphs;

        System.out.println("Connecting to the Database");
        connect = DriverManager.getConnection(DB_URL2,username,paswd);

        System.out.println("Successfully Connected");
        statement = connect.createStatement();

        int i =0;
        Graph graph1;
        CrimeNode crime;

        dropTables(statement);

        while (i < gr.size())
        {
             Graph cluster = gr.get(i);
             String graphName = "graph"+i;
             String sql = "CREATE TABLE "+graphName+
                       "( INCIDENT_DAY VARCHAR(255), " +
                       " INCIDENT_TIME VARCHAR(255), " + 
                       " VICTIM VARCHAR(255), " + 
                       " SUSPECT VARCHAR(255), " + 
                       " METHOD_VICTIM_CAPTURE VARCHAR(255), " + 
                       " SUBSTANCE_ABUSE_SUSPECTED VARCHAR(255), " + 
                       " SUSPECT_DISGUISED VARCHAR(255), " + 
                       " SUSPECT_AGE VARCHAR(255), " + 
                       " INCIDENT_LOC VARCHAR(255), " + 
                       " VICTIM_AGE VARCHAR(255) ) " ; 

             //Assuming that a graph is an arraylist

             //For each CrimeNode in the graph;
                    //System.out.println(statement.executeUpdate(sql));

                    statement.executeUpdate(sql);

                    ArrayList<CrimeNode> ClusterCrimes = cluster.getCrimeNodes();

                sQl = "SELECT * FROM "+graphName;

                    //System.out.println(statement.executeQuery(sQl));
                statement.executeQuery(sQl);

                graphTable =statement.executeQuery(sQl);

                for (int j=0; j< ClusterCrimes.size(); j++)
                {
                        CrimeNode crime1 = ClusterCrimes.get(j);

                    String data = "INSERT INTO "+graphName+"( INCIDENT_DAY ,INCIDENT_TIME ,VICTIM," +
                                " SUSPECT , METHOD_VICTIM_CAPTURE ,SUBSTANCE_ABUSE_SUSPECTED ," +
                                "SUSPECT_DISGUISED ,SUSPECT_AGE ,INCIDENT_LOC ," +
                                "VICTIM_AGE ) VALUES ( '"+crime1.incident_day+"', '"+
                                   crime1.incident_time+"', '"+
                                   crime1.victim+"', '"+ 
                                   crime1.suspect+"', '"+ 
                                   crime1.method_capture+"', '"+ 
                                   crime1.substance_abuse+"', '"+ 
                                   crime1.suspect_disguised+"', '"+ 
                                   crime1.suspect_age+"', '"+ 
                                   crime1.incident_loc +"', '"+ 
                                   crime1.victim_age+"' )"; 
                    //System.out.println(data);

                    //System.out.println(statement.executeUpdate(data));
                    statement.executeUpdate(data);
                }

                i++;
        }

            //takes an arraylist of HCS graphs
            //For each graph, get the Arraylist of CrimeNodes and create tables
            //Save tables on remote database
    }

    public static void dropTables(Statement statement){
            System.out.println("Started deleting tables ...");
            int i = 0;
            loop:
            while (true){

                    try{
                            String sql = "DROP TABLE graph"+i;
                            statement.executeUpdate(sql);
                    }catch(Exception e){
                            break loop;
                    }
                    i++;
            }

            System.out.println("Finished deleting tables ...");

    }
}