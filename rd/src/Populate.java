/*
**Populate Servlet
**Fills in the database with information entered from the crime form
**Tinashe Madzingaidzo, MDZTIN003
**20 September 2015
*/



import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
//import javax.jms.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

public class Populate extends HttpServlet{

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
                                     throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        String connURL = "jdbc:mysql://127.0.0.1:3306/demodb";// demodb is the database
        Connection conn;
        try{
          String incident_day = request.getParameter("Incident day"); 
          if (incident_day==null){
            incident_day="";
          }
          String incident_time = request.getParameter("Incident time");
          if (incident_time==null){
            incident_time="";
          }
          String victim = request.getParameter("Victim");
          if (victim==null){
            victim="";
          }
          String victim_age = request.getParameter("Victim age");
          if (victim_age==null){
            victim_age="";
          }
          String suspect = request.getParameter("Suspect");
          if (suspect==null){
            suspect="";
          }
          String suspect_age = request.getParameter("Suspect age");
          if (suspect_age==null){
            suspect_age="";
          }
          String method_victim_capture = request.getParameter("Method victim capture");
          if (method_victim_capture==null){
            method_victim_capture="";
          }
          String substance_abuse_suspected = request.getParameter("Substance abuse suspected");
          if (substance_abuse_suspected==null){
            substance_abuse_suspected="";
          }
          String suspect_disguised = request.getParameter("Suspect disguised");
          if (suspect_disguised==null){
            suspect_disguised="";
          }
          String incident_loc = request.getParameter("Incident Location");
          if (incident_loc==null){
            incident_loc="";
          }

          Class.forName("com.mysql.jdbc.Driver");
          conn = DriverManager.getConnection(connURL, "root", "");
          PreparedStatement statement = conn.prepareStatement("insert into Data(incident_day,incident_time,victim,victim_age,suspect,suspect_age,method_victim_capture,substance_abuse_suspected,suspect_disguised,incident_loc) values(?,?,?,?,?,?,?,?,?,?)");//Data is the name of the table
          statement.setString(1,incident_day);
          statement.setString(2,incident_time);      
          statement.setString(3,victim);
          statement.setString(4,victim_age);
          statement.setString(5,suspect);
          statement.setString(6,suspect_age);
          statement.setString(7,method_victim_capture);
          statement.setString(8,substance_abuse_suspected);
          statement.setString(9,suspect_disguised);
          statement.setString(10,incident_loc);

          int i = statement.executeUpdate();
          if(i!=0){
            writer.println("<br>Successfully inserted!");
          }
          else{
            writer.println("Failed to insert");
           }
        }
        catch (Exception e){
          writer.println(e);
        }
      }
}
