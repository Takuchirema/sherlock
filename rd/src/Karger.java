import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

public class Karger extends RecursiveTask<CutGraph> {
	private Graph graph;
	private int graphSize;
	
	public Karger(Graph graph,int graphSize){
		this.graph = graph;
		this.graphSize = graphSize;
	}
	
	@Override
	protected CutGraph compute(){
		
		/* has the super nodes of minimum cut up to a certain point in the tree */
		CutGraph cutGraph = new CutGraph();
		
		Random rand = new Random();
		int size = graph.edges.size();
		
		int superNodesNum = graph.getDistances().size();
		
		if (superNodesNum < Math.ceil(graphSize/(Math.sqrt(2.0))) ){
			
			System.out.println(" \n ^^^^^^^^^^ split ^^^^^^^^^^ "+graphSize+" "+superNodesNum);
			
			Graph cloned1 = cloneGraph (graph);
			Graph cloned2 = cloneGraph (graph);
			
			Karger karger1 = new Karger (cloned1,cloned1.getDistances().size());
			Karger karger2 = new Karger (cloned2,cloned2.getDistances().size());
			
			karger1.fork();
			
			CutGraph cutGraph2 = karger2.compute();
			CutGraph cutGraph1 = karger1.join();

			/* The values to be returned */
			if (cutGraph1.getMinCut() < cutGraph2.getMinCut()){
				cutGraph = cutGraph1;
			}else{
				cutGraph = cutGraph2;
			}
		}else if (size>1){
			
			ArrayList<CrimeNode> crimes = graph.getCrimeNodes();
			
			System.out.println(" edges size "+size);
			/* Not inclusive */
			int randomNum = rand.nextInt( size - 1 );
			
			/* change it so that we are not left with singletons */
			if (graph.getCrimeNodes().size() < 150 ){
				randomNum = group(graph);
			}
			
			int[] edge = graph.edges.get(randomNum);
			
			
			ArrayList<ArrayList<Double> > distances = graph.getDistances();
			
			int joined = edge[1];
			int kept = edge[0];
			
			int joinedId = crimes.get(edge[1]).id;
			int keptId = crimes.get(edge[0]).id;
			
			System.out.println(" joinedId "+joinedId+" "+crimes.get(joined).MasterNode.size()+" keptId "+keptId+" "
					+crimes.get(kept).MasterNode.size());
			
			/* the first node in the edge is kept the other discarded */
			CrimeNode keptNode = graph.getCrimeNodes().get(edge[0]);
			keptNode.MasterNode.addAll( graph.getCrimeNodes().get(edge[1]).MasterNode );
			
			 graph.getCrimeNodes().set(edge[0], keptNode);
			
			
			System.out.println(" dists "+graph.getDistances().size()+" joined "+joined+" kept "+kept+" edges "+graph.edges.size());
			
			
			for (int i=0;i<distances.size();i++){
				
				/* set an edge from kept node to neighbor of removed */
				if (i != kept && distances.get(joined).get(i) >0){
					/* check if from i to kept there is a connection */
					double edgeCount = distances.get(joined).get(i);
					if (distances.get(i).get(kept) > 0)
						edgeCount = distances.get(joined).get(i) + distances.get(i).get(kept);
					
					distances.get(i).set(kept, edgeCount);
					distances.get(kept).set(i, edgeCount);
				}
				
			}
			
			for (int i=0;i<distances.size();i++){
						
				/* Then remove the item */
				distances.get(i).remove(joined);	
				//System.out.println(" len of arrinner "+distances.get(i).size());
				
			}
			
			
			/* then remove the row */
			distances.remove(joined);
			
			System.out.println(" dist is "+distances.size()+" "+distances.get(0).size());
			//printDistances(distances);
			
			ArrayList<int[]> newEdges = new ArrayList<int[]>();
			
			/* redo the edges */
			for (int i=0;i<distances.size();i++){
				for (int j=i;j<distances.size();j++){
					
					if (distances.get(i).get(j) > 0){
						newEdges.add(new int[]{i,j});
					}
				}
			}
			
			/* replace the edge */
			graph.edges = newEdges;
			
			graph.getCrimeNodes().remove(joined);
			
			//if (graph.getCrimeNodes().size() > 2){
			if (graph.edges.size() > 1){
				
				Karger karger = new Karger (graph,graphSize);
				return karger.compute();
				
			}else{
				
				//printGraph(graph);
				
				double mincut = 1;
				
				for (int i=0;i<graph.getDistances().size();i++){
					if (graph.getDistances().get(i).get(0) >0){
						mincut = graph.getDistances().get(i).get(0);
					}
				}
				
				cutGraph = new CutGraph(graph.getCrimeNodes(),mincut);
			}
		 
		}else{
			double mincut = 1;
			
			for (int i=0;i<graph.getDistances().size();i++){
				if (graph.getDistances().get(i).get(0) >0){
					mincut = graph.getDistances().get(i).get(0);
				}
			}
			
			cutGraph = new CutGraph(graph.getCrimeNodes(),mincut);
		}
		
		return cutGraph;
		
	}
	
	public static Graph cloneGraph(Graph graph){
		Graph clone = new Graph ();
		
		ArrayList<ArrayList<Double>> distances = graph.getDistances();
		
		ArrayList<ArrayList<Double>> newDistances = new ArrayList<ArrayList<Double>>();
		
		ArrayList<int[]> edges = graph.edges;
		
		ArrayList<int[]> newEdges = new ArrayList<int[]>();
		
		ArrayList<CrimeNode> crimeNodes = graph.getCrimeNodes();
		
		ArrayList<CrimeNode> newCrimeNodes = new ArrayList<CrimeNode>();
		
		
		for (int i=0;i<distances.size();i++){
			ArrayList<Double> nodes = new ArrayList<Double>();
			
			for (int j=0;j<distances.size();j++){
				nodes.add(distances.get(i).get(j));
			}
			newDistances.add(nodes);
		}
		
		for (int i=0;i<edges.size();i++){
			int[] edge = new int[]{edges.get(i)[0],edges.get(i)[1]};
			newEdges.add(edge);
		}
		
		for (int i=0;i<crimeNodes.size();i++){
			
			CrimeNode oldcrime = crimeNodes.get(i);
			
			CrimeNode newcrime = new CrimeNode(oldcrime.id);
			
			newcrime.incident_day = oldcrime.incident_day;
			newcrime.incident_time = oldcrime.incident_time ;
			newcrime.victim = oldcrime.victim;
			newcrime.suspect = oldcrime.suspect;
			newcrime.victim_age = oldcrime.victim_age;
			newcrime.suspect_age = oldcrime.suspect_age;
			newcrime.method_capture = oldcrime.method_capture;
			newcrime.substance_abuse = oldcrime.substance_abuse;
			newcrime.suspect_disguised = oldcrime.suspect_disguised;
			newcrime.incident_loc = oldcrime.incident_loc;
			newcrime.connected = oldcrime.connected;
                        
                        newcrime.lat = oldcrime.lat;
			newcrime.lng = oldcrime.lng;
			newcrime.type = oldcrime.type;
			
			ArrayList<CrimeNode> supernode = new ArrayList<CrimeNode>();
			
			for (int j=0;j<oldcrime.MasterNode.size();j++){
				CrimeNode node = new CrimeNode(oldcrime.MasterNode.get(j).id);
				
				node.incident_day = oldcrime.MasterNode.get(j).incident_day;
				node.incident_time = oldcrime.MasterNode.get(j).incident_time ;
				node.victim = oldcrime.MasterNode.get(j).victim;
				node.suspect = oldcrime.MasterNode.get(j).suspect;
				node.victim_age = oldcrime.MasterNode.get(j).victim_age;
				node.suspect_age = oldcrime.MasterNode.get(j).suspect_age;
				node.method_capture = oldcrime.MasterNode.get(j).method_capture;
				node.substance_abuse = oldcrime.MasterNode.get(j).substance_abuse;
				node.suspect_disguised = oldcrime.MasterNode.get(j).suspect_disguised;
				node.incident_loc = oldcrime.MasterNode.get(j).incident_loc;
				node.connected = oldcrime.MasterNode.get(j).connected;
                                
                                node.lat = oldcrime.MasterNode.get(j).lat;
				node.lng = oldcrime.MasterNode.get(j).lng;
				node.type = oldcrime.MasterNode.get(j).type;
				
				supernode.add(node);
			}
			
			newcrime.MasterNode = supernode;
			newCrimeNodes.add(newcrime);
		}
		
		clone = new Graph (newCrimeNodes,newDistances);
		clone.edges = newEdges;
		
		return clone;
	}
	
	public static void printGraph(Graph graph){
		
		ArrayList<CrimeNode> crimes = graph.getCrimeNodes();
		
		for (int i=0;i<crimes.size();i++){
			System.out.println((i+1)+
					String.format("%0$5s", ". ID: "+crimes.get(i).id)+
					String.format("%0$25s", ", Location: "+crimes.get(i).incident_loc)+
					String.format("%0$17s",", Day :"+crimes.get(i).incident_day)+
					String.format("%0$14s",", Time :"+crimes.get(i).incident_time) );
		}
	}
	
	public static void printDistances(ArrayList<ArrayList<Double>> dist){
		System.out.println("");
		for (int i=0;i<dist.size();i++){
			String row = "";
			for (int j=0;j<dist.size();j++)
				row = row +" "+ dist.get(i).get(j);
			System.out.println(row);
		}
		System.out.println("");
	}
	
	public static int group(Graph graph){
		int edge = 0;
		int sum = 0;
		
		/* take an edge with lowest sums */
		for (int i=0;i<graph.edges.size();i++){
			CrimeNode crime1 = graph.getCrimeNodes().get(graph.edges.get(i)[0]);
			CrimeNode crime2 = graph.getCrimeNodes().get(graph.edges.get(i)[1]);
			
			int sum1= crime1.MasterNode.size() + crime2.MasterNode.size();
			if (sum == 0){
				sum = sum1;
			}else if(sum1 <sum){
				edge = i;
				sum = sum1;
			}
		}
		
		return edge;
	}

}
