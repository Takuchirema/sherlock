/*
**Distance Matrix class to create the graphs to be used in the HCS implementation.
**This class creates the edges between crime nodes, based on the following attributes:
** -Location longitude and latitude coordinates; the day of the crime; and the time it happened
**
**Tinashe Madzingaidzo, MDZTIN003
*/

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

public class DistanceMatrix
{
    private static ArrayList<int[]> edgeConnectivity = new ArrayList<int[]>();
   
    private static ArrayList<CrimeNode> c = new ArrayList<CrimeNode>();
    private static Graph graph = new Graph();
    
    public static void getData()
    {
        ArrayList<String> crime_List = new ArrayList<>();
        
        CrimeCollection crimeData = new CrimeCollection();
        crimeData.createGraph();
        graph = crimeData.getGraph();
        c = crimeData.getCrimeNodes();
       
        graph = new Graph(c,calcDistances());
        
        graph.edges = edgeConnectivity;
    }
    
    
    public static Graph getGraph(){
    	
    	return graph;
    }
    public static ArrayList<ArrayList<Double>> calcDistances()
    {
        ArrayList<double[]> weights = new ArrayList<>();   //arraylist of arrays of weights to be used
        ArrayList<String[]> crimes = new ArrayList<>();    //arraylist of the crimes' useful attributes
        String numbers = "";
        double dist = 0;
        
        int size = c.size();
        
        ArrayList<ArrayList<Double> > edges = new ArrayList<ArrayList<Double> >();
        String day = "";
        String time = "";
        String location = "";
        
        /*  ********  */
        String method_capture = "";
        String motivation = "";
        String suspect_frame = "";
        
        String type="";
        
		
        for(CrimeNode entry:c)
        {
            
            
            day = entry.incident_day;
            time = entry.incident_time;
            type = entry.type;
            
            crimes.add(new String[]{day,time,type,entry.lat,entry.lng});
            time = "";
            day = "";
            location = "";
            
            method_capture = "";
            motivation = "";
            suspect_frame = "";
        }
        
        for(String[] item:crimes)
        {
            
            double c1 = Double.parseDouble(item[3]);
            double c2 = Double.parseDouble(item[4]);
            double weightedVars[] = new double[9];
            weightedVars[0] = c1;            //lattitude
            weightedVars[1] = c2;            //longitude
            switch(item[0])
            {
                case "Monday":                       //weighted days
                    weightedVars[2]= 0;
                    weightedVars[3]= 25;
                    break;
                case "Tuesday":
                    weightedVars[2]= 19.55;
                    weightedVars[3]= 15.59;
                    break;
                case "Wednesday":
                    weightedVars[2]= 24.38;
                    weightedVars[3]= 5.56;
                    break;
                case "Thursday":
                    weightedVars[2]= 10.85;
                    weightedVars[3]= -22.53;
                    break;
                case "Friday":
                    weightedVars[2]= -10.85;
                    weightedVars[3]= -22.53;
                    break;
                case "Saturday":
                    weightedVars[2]= -24.38;
                    weightedVars[3]= -5.56;
                    break;
                case "Sunday":
                    weightedVars[2]= -19.55;
                    weightedVars[3]= 15.59;
                    break;
            }
            switch(item[1])                            
            {
                case "Morning":                           //weighted times
                    weightedVars[4]= 0;
                    weightedVars[5]= 25;
                    break;
                case "Noon":
                    weightedVars[4]= 25;
                    weightedVars[5]= 0;
                    break;
                case "Evening":
                    weightedVars[4]= 0;
                    weightedVars[5]= -25;
                    break;
                case "Night":
                    weightedVars[4]= -25;
                    weightedVars[5]= 0;
                    break;
            }
            
            switch(item[2])                            
            {
                /*BATTERY
                    THEFT
                    ROBBERY
                    LIQUOR LAW VIOLATION
                    NARCOTICS
                    CRIMINAL DAMAGE
                    DECEPTIVE PRACTICE
                    ASSAULT
                    MOTOR VEHICLE THEFT
                    BURGLARY
                    OTHER OFFENSE
                    WEAPONS VIOLATION
                    CRIMINAL TRESPASS
                    OFFENSE INVOLVING CHILDREN
                    PUBLIC PEACE VIOLATION
                    HOMICIDE*/
                
                case "BATTERY":                          
                    weightedVars[6]= 1;
                    break;
                case "THEFT":
                    weightedVars[6]= 2;
                    break;
                case "ROBBERY":
                    weightedVars[6]= 3;
                    break;
                case "LIQUOR LAW VIOLATION":
                    weightedVars[6]= 4;
                    break;
                case "NARCOTICS":                         
                    weightedVars[6]= 5;
                    break;
                case "CRIMINAL DAMAGE":
                    weightedVars[6]= 6;
                    break;
                case "DECEPTIVE PRACTICE":
                    weightedVars[6]= 7;
                    break;
                case "ASSAULT":
                    weightedVars[6]= 8;
                    break;
                case "MOTOR VEHICLE THEFT":
                    weightedVars[6]= 9;
                    break;
                case "BURGLARY":                         
                    weightedVars[6]= 10;
                    break;
                case "OTHER OFFENSE":
                    weightedVars[6]= 11;
                    break;
                case "WEAPONS VIOLATION":
                    weightedVars[6]= 12;
                    break;
                case "CRIMINAL TRESPASS":
                    weightedVars[6]= 13;
                    break;
                case "OFFENSE INVOLVING CHILDREN":
                    weightedVars[6]= 14;
                    break;
                case "PUBLIC PEACE VIOLATION":                         
                    weightedVars[6]= 15;
                    break;
                case "HOMICIDE":
                    weightedVars[6]= 16;
                    break;             
            }
            
            
            weights.add(weightedVars);    
        }
       
        double diff=0;
        int count=0;
        double totRow=0;
        double meanRow=0;
        double stdv=0;
        double sd=0;
        
        /******************Standardiztion of all critical attributes**************/
          for(count =0;count<7;count++)
          {
              for(int i=0;i<weights.size();i++)
              {
                  totRow+=weights.get(i)[count];          //sum of attribute weights.get(i)[count] for all crimes
              }
              meanRow=totRow/weights.size();              //mean of attribute weights.get(i)[count] for all crimes
              //System.out.println("mean "+meanRow);
              for(int j=0;j<weights.size();j++)
              {
                  diff+=Math.pow((weights.get(j)[count] - meanRow),2);
              }
              stdv=Math.sqrt(diff/(weights.size()-1));   //standard deviation of the attribute
              //System.out.println("std "+stdv);
              for(int k=0;k<weights.size();k++)
              {
                  sd=(weights.get(k)[count]-meanRow)/stdv; //standardized value of the attribute
                  
                  if (count == 0 || count == 1)
                      sd = sd*100;                      //the coordinate attributes are the most important
                                                        //hence multiplied by 100
                  //System.out.println("sd "+sd);
                  weights.get(k)[count]=sd;
              }
              totRow=0;                                //reset counters
              diff=0;
          }
          
          
          double dif=0;
          double diffSqre=0;
          double diffTot = 0;
          double answer = 0;
          double sum=0;
          
          for(int l=0;l<weights.size();l++)
          {
        	  ArrayList<Double> node = new ArrayList<Double>();
          	  edges.add(node);
          	  
              for(int m=0;m<weights.size();m++)
              {
                  for(count=0;count<7;count++)
                  {
                      
                      
                      dif= weights.get(l)[count] - weights.get(m)[count];//Calculate distance between
                      //System.out.println("diff "+dif+" rows "+count);
                      diffSqre = Math.pow(dif, 2);                        //crime nodes
                      diffTot+=diffSqre;
                      
                  }
                  answer = Math.sqrt(diffTot);
                  
                  if(0.0<answer && answer>12)               //set cut-off to determine connectivity
                 {
                	
                     edges.get(l).add(m, -1.0);
                    
                    edgeConnectivity.add(new int[]{l,m});

                }else{ 
                
                	if (answer ==0){
                		edges.get(l).add(m, 0.0);
                	}
                	else{
                		edges.get(l).add(m, 1.0);
                	}
                }
                  diffTot=0;
              }
          }
        
        return edges;
    }
    
  
}
